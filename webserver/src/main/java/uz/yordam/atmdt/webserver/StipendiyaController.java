package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class StipendiyaController
{
    public  void Uch(RoutingContext context)
    {
        context.response().end("Uchlik stipendiya 350000 so'm");
    }

    public void Tort(RoutingContext context)
    {
        context.response().end("To'rtlik stipendiya 550000 so'm");
    }
    public void Besh(RoutingContext context)
    {
        context.response().end("Beshlik stipendiya 750000 so'm. Shuning uchun 5 ga o'qish kerak, student!");
    }
}