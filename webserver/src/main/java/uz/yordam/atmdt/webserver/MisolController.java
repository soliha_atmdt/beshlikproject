package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class MisolController {
    public void faktor(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        int a1 = Integer.parseInt(son1);
        int s=1;
        for (int i=0;i<=a1;i++){
            s=s*i;
        }

        routingContext.response().end(a1+"faktorial"+s+"ga teng");
    }
    public void yosh(RoutingContext routingContext){
        String son1= routingContext.request().params().get("son1");
        int a1 = Integer.parseInt(son1);
        if (a1<16){
            routingContext.response().end("O`smir boladi"+a1);
        }
        if (16<a1&&a1<30){
            routingContext.response().end("Yosh boladi"+a1);
        }
        if (30<a1&&a1<45){
            routingContext.response().end("O'rta yosh boladi"+a1);
        }
        if (45<a1){
            routingContext.response().end("Keksa boladi"+a1);
        }
    }

}
