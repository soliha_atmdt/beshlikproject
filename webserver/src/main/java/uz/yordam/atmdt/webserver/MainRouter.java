package uz.yordam.atmdt.webserver;

import io.vertx.core.Vertx;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;


public class MainRouter {
    private MainController mainController;
    private NomerController nomerController;
    private MatemController matemController;
    private AkaController akaController;
    private UkaController ukaController;
    private OpaController opaController;
    private SingilController singilController;
    private YigindiController yigindiController;
    private dynamicController dinamikController;
    private archaController archaController;
    private KopaytmaController kopaytmaController;
    private DarajaController darajaController;
    private StipendiyaController stipendiyaController;
    private IndexController indexController;


    public MainRouter(){
        mainController=new MainController();
        nomerController=new NomerController();
        matemController=new MatemController();
        akaController=new AkaController();
        ukaController=new UkaController();
        opaController=new OpaController();
        singilController=new SingilController();
        yigindiController=new YigindiController();
        dinamikController=new dynamicController();
        archaController=new archaController();
        kopaytmaController=new KopaytmaController();
        darajaController=new DarajaController();
        stipendiyaController=new StipendiyaController();
        indexController=new IndexController();
    }
    public Router getRouter(Vertx vertx){
        Router router=Router.router(vertx);
        router.route("/assets/*").handler(StaticHandler
                .create()
                .setCachingEnabled(true)
                .setWebRoot("assets"));
        router.route().handler(BodyHandler.create());
        //statik controllerlar
        router.route("/shapka").handler(mainController::Shapka);
        router.route("/perchatka").handler(mainController::Perchatka);

        router.route("/bilayn").handler(nomerController::Bilayn);
        router.route("/ucell").handler(nomerController::Ucell);

        router.route("/osimliklar").handler(akaController::Akajon);
        router.route("/hayvonlar").handler(akaController::Akam);

        router.route("/ukajon").handler(ukaController::Ukajon);
        router.route("/ukam").handler(ukaController::Ukam);

        router.route("/mehribon").handler(opaController::Mehribon);
        router.route("/aqlli").handler(opaController::Aqlli);

        router.route("/hurmat").handler(singilController::Hurmat);
        router.route("/chiroyli").handler(singilController::Chiroyli);

        router.route("/oqituvchi").handler(matemController::Oqituvchi);
        router.route("/oylik").handler(matemController::Oylik);


        //dinamik controllerlar
        router.route().handler(BodyHandler.create());
        router.route("/yigindi/:son1/:son2").handler(yigindiController::yigindi);
        router.route("/archa/:son1/:son2").handler(archaController::metod1);
        router.route("/dinamik/:son1").handler(dinamikController::metod1);
        router.route("/kopaytma/:son1/:son2").handler(kopaytmaController::kopaytma);
        router.route("/daraja/:son1/son2").handler(darajaController::daraja);
        router.route("/stipendiya/uch").handler(stipendiyaController::Uch);
        router.route("/stipendiya/tort").handler(stipendiyaController::Tort);
        router.route("/stipendiya/besh").handler(stipendiyaController::Besh);
        router.route("/index").handler(indexController::indexPage);


        router.route().handler(this::page404);
        return router;
    }
    public Router makeRouter(Vertx vertx){
        Router router=Router.router(vertx);
        router.route().handler(BodyHandler.create());
        router.route("/yigindi/:son1/:son2").handler(yigindiController::yigindi);
        router.route("/kopaytma/:son1/:son2").handler(kopaytmaController::kopaytma);
        router.route("/daraja/:son1/:son2").handler(darajaController::daraja);
        router.route("/stipendiya/uch").handler(stipendiyaController::Uch);
        router.route("/stipendiya/tort").handler(stipendiyaController::Tort);
        router.route("/stipendiya/besh").handler(stipendiyaController::Besh);
        return router;
    }
    public void page404(RoutingContext routingContext){
        routingContext
                .response()
                .setStatusCode(404)
                .end("xato");
    }
}

