package uz.yordam.atmdt.webserver;

import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine;

public class IndexController {

    private FreeMarkerTemplateEngine templateEngine;
    public IndexController(){
        this.templateEngine=FreeMarkerTemplateEngine.create(Vertx.vertx());
    }


    public void indexPage(RoutingContext routingContext){
        JsonObject data=new JsonObject()
                .put("title","Bosh sahifa")
                .put("index","Bosh sahifa");

        templateEngine.render(data,"templates/index.ftl",res->{
            if (res.succeeded()) {
                routingContext.response().end(res.result());
            } else {
                routingContext.fail(res.cause());
            }
        });
    }
}
