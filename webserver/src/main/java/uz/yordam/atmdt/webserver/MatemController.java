package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class MatemController {
    public void Oqituvchi(RoutingContext routingContext)
    {
        routingContext.response()
                .end("Matematika oqituvchisi bolish juda qiyin 25000");
    }

    public void Oylik(RoutingContext routingContext)
    {
        routingContext.response().end("Lekin shunga yarasha oyligi baland emas afsuski");
    }
}