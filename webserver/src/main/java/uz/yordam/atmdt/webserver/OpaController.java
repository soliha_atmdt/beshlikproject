package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class OpaController {
    public void Mehribon(RoutingContext routingContext)
    {
        routingContext.response()
                .end("Opalar doim mehribon bolishi kerak");
    }

    public void Aqlli(RoutingContext routingContext)
    {
        routingContext.response().end("Aqlli insonning sadagasi ketsang arziydi");
    }
}
