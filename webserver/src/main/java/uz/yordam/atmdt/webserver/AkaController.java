package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class AkaController {
    public void Akajon(RoutingContext routingContext)
    {
        routingContext.response()
                .end("Akajon desang eriydi qalbim");
    }

    public void Akam(RoutingContext routingContext)
    {
        routingContext.response().end("Akam desang sen uchun beraman jonim");
    }
}
