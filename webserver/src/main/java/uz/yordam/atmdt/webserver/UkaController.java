package uz.yordam.atmdt.webserver;

import io.vertx.ext.web.RoutingContext;

public class UkaController {
    public void Ukam(RoutingContext routingContext)
    {
        routingContext.response()
                .end("Ukajon akangizga salom deb qoying");
    }

    public void Ukajon(RoutingContext routingContext)
    {
        routingContext.response().end("Akaga hurmatda ukaga izzatda boling");
    }
}
